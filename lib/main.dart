import 'dart:html';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    _doRedirect();
    return Container();
  }

  Future _doRedirect() async {
    var url = window.location.href;
    if (await canLaunch(
        'http://localhost:8002/?url=https://www.facebook.com?showTabs=true')) {
      await launch(
          'http://localhost:8002/?url=https://www.facebook.com?showTabs=true');
      //window.close();
      print(window.closed);
    } else {
      throw 'Could not launch';
    }
  }
}
